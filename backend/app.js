const express = require('express');

const userRoutes = require('./routes/user.route');
const paramsRoutes = require('./routes/params.route');
const postsRoutes = require('./routes/posts.route');
const authRoutes = require('./routes/auth.route');
const path = require('path');

//cree une application express
const app = express();

//cors
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});



app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use('/images', express.static(path.join(__dirname, 'images')));

app.use('/api/auth', authRoutes);
app.use('/api/users', userRoutes);
app.use('/api/params', paramsRoutes);
app.use('/api/posts', postsRoutes);
//on exporte cette application pour pouvoir y acceder depuis nos autres fichiers du projet
module.exports = app;