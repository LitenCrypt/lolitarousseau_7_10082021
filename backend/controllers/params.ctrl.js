const { ParamsSvc } = require('../services/params.svc');

exports.ParamsCtrl = {
    /**
     * Récupére les services 
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    getServices: function(req, res, next) {
        try {
            // On intéroge les données
            ParamsSvc.getAllServices().then((r) => {
                    console.log(r);
                    // On transmet le resultat au front
                    res.status(200).json(r);
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch((e) => { res.status(500).json({ e }); });
        } catch (error) {
            //fatality
            return res.status(500).json({ error });
        }
    }
}