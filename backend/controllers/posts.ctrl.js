const { PostsSvc } = require('../services/posts.svc');
/**
 * Exposition des Posts et des actions possibles
 */
exports.PostCtrl = {
    /**
     * R2cupération de tous les posts
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    get: function(req, res, next) {
        console.log('get Posts------------------------------------');
        try {
            // On intéroge les données
            PostsSvc.getPosts().then((r) => {
                    // On transmet le resultat au front
                    res.status(200).json({ posts: r });
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch((e) => { res.status(500).json({ e }); });
        } catch (e) {
            //fatality
            res.status(500).json({ e });
        }
        console.log('get Posts------------------------------------');
    },
    /**
     * Récupération d'un post
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    getOne: function(req, res, next) {
        console.log('getOne Posts------------------------------------');
        try {
            // On intéroge les données
            PostsSvc.getPost(req.params.id).then((r) => {
                    // On transmet le resultat au front
                    res.status(200).json({ post: r });
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch((e) => { res.status(500).json({ e }); });
        } catch (e) {
            //fatality
            res.status(500).json({ e });
        }
        console.log('getOne Posts------------------------------------');
    },
    /**
     * Mise à jour d'un post
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    updateOne: function(req, res, next) {
        console.log('updateOne Posts------------------------------------');
        try {
            console.log(req.body.post);
            //on récupére l'objet post passé dans le formulaire de la requete
            // on le transforme en objet
            const p = JSON.parse(req.body.post);
            console.log(p);
            // On remplace la valeur de la propriété portant le fichier associé 
            // avec le chemin HTTP pour y accéder sur le serveur
            p.GifUrl = `${req.protocol}://${req.get('host')}/images/${req.file.filename}`;
            console.log(p.GifUrl);
            // On modifie les données
            PostsSvc.updatePost(req.body.post).then((r) => {
                    // On transmet le resultat au front
                    res.status(200).json(r);
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch((e) => { res.status(500).json('Erreur lors de la mise à jour: ' + e); });
        } catch (e) {
            //fatality
            res.status(500).json({ e });
        }
        console.log('updateOne Posts------------------------------------');
    },
    /**
     * Ajout d'un post
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    insertOne: function(req, res, next) {
        console.log('insertOne Posts------------------------------------');
        try {
            console.log(req.body.post);
            //on récupére l'objet post passé dans le formulaire de la requete
            // on le transforme en objet
            const p = JSON.parse(req.body.post);
            console.log(p);
            // On remplace la valeur de la propriété portant le fichier associé 
            // avec le chemin HTTP pour y accéder sur le serveur
            p.GifUrl = `${req.protocol}://${req.get('host')}/images/${req.file.filename}`;
            console.log(p.GifUrl);
            // On modifie les données
            PostsSvc.createPost(p).then((r) => {
                    console.log(r);
                    // On transmet le resultat au front
                    res.status(200).json(r);
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch((e) => { res.status(500).json('Erreur lors de la création: ' + e); });
        } catch (e) {
            //fatality
            res.status(500).json({ e });
        }
        console.log('insertOne Posts------------------------------------');
    },
    /**
     * Supression d'un post
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    deleteOne: function(req, res, next) {
        console.log('deleteOne Posts------------------------------------');
        try {
            console.log(req.param.id);
            // On modifie les données
            PostsSvc.deletePost(req.params.id).then((r) => {
                    console.log(r);
                    // On transmet le resultat au front
                    res.status(200).json(r);
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch((e) => { res.status(500).json('Erreur lors de la suppression: ' + e); });
        } catch (e) {
            //fatality
            res.status(500).json({ e });
        }
        console.log('deleteOnePosts------------------------------------');
    },
    /**
     * Récupération de tous les commentaires d'un post
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    getComments: function(req, res, next) {
        console.log('getComments Posts------------------------------------');
        try {
            // On intéroge les données
            PostsSvc.getPostComments(req.params.id).then((r) => {
                    // On transmet le resultat au front
                    res.status(200).json({ comments: r });
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch((e) => { res.status(500).json({ e }); });
        } catch (e) {
            //fatality
            res.status(500).json({ e });
        }
        console.log('getComments Posts------------------------------------');
    },
    /**
     * Ajout d'un commentaire à un post
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    addComment: function(req, res, next) {
        console.log('addComment Posts------------------------------------');
        try {
            console.log(req.body.comment);
            // On modifie les données
            PostsSvc.addPostComments(req.body.comment).then((r) => {
                    // On transmet le resultat au front
                    res.status(200).json({ comments: r });
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch((e) => { res.status(500).json({ e }); });
        } catch (e) {
            //fatality
            res.status(500).json({ e });
        }
        console.log('addComment Posts------------------------------------');
    },
    /**
     * Supression d'un commentaire
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    deleteComment: function(req, res, next) {
        console.log('deleteComment Posts------------------------------------');
        try {
            console.log(req.param.cid);
            // On modifie les données
            // on passe l'id transmit du front
            PostsSvc.deletePostsComment(req.params.cid, ).then((r) => {
                    // On transmet le resultat au front
                    res.status(200).json({ comments: r });
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch((e) => { res.status(500).json({ e }); });
        } catch (e) {
            //fatality
            res.status(500).json({ e });
        }
        console.log('deleteComment Posts------------------------------------');
    },
}