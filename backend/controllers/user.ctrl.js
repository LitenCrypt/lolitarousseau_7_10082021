const bcrypt = require('bcrypt');
const { UsrSvc } = require('../services/users.svc');
const jwt = require('jsonwebtoken');

/**
 * Exposition des utilisateurs et des actions possibles
 */
exports.UserCtrl = {
    /**
     * Inscription des utilisateurs
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    signup: function(req, res, next) {
        console.log('signup Users: ------------------------------------');
        console.log(req.body.user);
        // On récupére l'utilisateur passé dans le FormData de la requete
        // et on le transforme en objet (de String > Object)
        const user = JSON.parse(req.body.user);
        // Stocke le mdp
        const pwd = user.Pwd;
        console.log(user);
        //hacher le mdp
        bcrypt.hash(pwd, 10).then(hash => {
                // le hash est utilisé comme pswd
                user.Pwd = hash;
                // on réupére l'image de l'avatar posté avec l'utilisateur
                user.ImgUrl = `${req.protocol}://${req.get('host')}/images/${req.file.filename}`;
                console.log(user.ImgUrl);
                //enregistre l'utilisateur dans le bdd
                UsrSvc.createUser(user).then((x) => {
                        // transmet l'utilisateur au front
                        // tranmet le token d'authent fourni pour l'utilisateur
                        // retourne code 200:OK
                        res.status(200).json({
                            user: x,
                            token: jwt.sign({ login: x.UserId }, 'RANDOM_TOKEN_SECRET', { expiresIn: '24h' })
                        });
                    }) // en cas d'erreur on renvois une erreur 500 au front
                    .catch((e) => {
                        console.log(error);
                        res.status(500).json({ e });
                    });
            }) // en cas d'erreur on renvois une erreur 500 au front
            .catch(error => {
                console.log(error);
                res.status(500).json({ error });
            });
        console.log('signup Users: ------------------------------------');
    },
    /**
     * Authentification des utilisateurs
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    login: function(req, res, next) {
        console.log('login Users: ------------------------------------');
        try {
            // on récupére l'ID utilisateur passé dans la requetes
            var userId = Buffer.from(req.body.login).toString('base64');
            console.log(userId);
            // On récupére la valeur du mot de passe dans la BDD pour ce login
            UsrSvc.getUserPswd(userId).then((x) => {
                    // on compare avec la valeur passé en paramétre
                    bcrypt.compare(req.body.pwd, x).then(valid => {
                            // Si la comparaison retourne faux
                            if (!valid) {
                                // Renvois une erreur 401:Non identifié/autorisé
                                return res.status(401).json({ error: 'Mot de passe invalide' });
                            } else {
                                // on récupére l'utilisateur
                                UsrSvc.getUser(userId).then((r) => {
                                        // on le renvois au front
                                        // on renvois le token d'authent
                                        res.status(200).json({
                                            user: r,
                                            token: jwt.sign({ login: r.UserId }, 'RANDOM_TOKEN_SECRET', { expiresIn: '24h' })
                                        });
                                    }) // en cas d'erreur on renvois une erreur 500 au front
                                    .catch((e) => { res.status(500).json({ error: e }); });
                            }
                        }) // en cas d'erreur on renvois une erreur 500 au front
                        .catch(error => res.status(401).json({ error: 'Mot de passe invalide' }));
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch(error => res.status(401).json({ error: 'Mot de passe invalide' }));
        } catch (e) {
            //fatality
            res.status(500).json({ e });
        }
        console.log('login Users: ------------------------------------');
    },
    /**
     * Récupération de la liste des utilisateurs
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    get: function(req, res, next) {
        console.log('get Users: ------------------------------------');
        try {
            // On intéroge les données
            UsrSvc.getAllUsers().then((r) => {
                    // On transmet le resultat au front
                    res.status(200).json({ users: r });
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch((e) => { res.status(500).json({ e }); });
        } catch (e) {
            //fatality
            res.status(500).json({ e });
        }
        console.log('get Users: ------------------------------------');
    },
    /**
     * Récupération de l'utilisateur
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    getOne: function(req, res, next) {
        console.log('getOne Users: ------------------------------------');
        try {
            // On intéroge les données
            UsrSvc.getUser(req.params.id).then((r) => {
                    // On transmet le resultat au front
                    res.status(200).json(r);
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch((e) => { res.status(500).json({ e }); });
        } catch (e) {
            //fatality
            res.status(500).json({ e });
        }
        console.log('getOne Users: ------------------------------------');
    },
    /**
     * Mise à jour de l'utilisateur
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     * @deprecated Cette fonction est non utilisées 
     */
    updateOne: function(req, res, next) {
        console.log('updateOne: ------------------------------------');
        try {
            // On modifie les données
            UsrSvc.updateUser(req.body.user).then((r) => {
                    // On transmet le resultat au front
                    res.status(200).json(r);
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch((e) => { res.status(500).json({ e }); });
        } catch (e) {
            //fatality
            res.status(500).json({ e });
        }
        console.log('updateOne: ------------------------------------');
    },
    /**
     * Récupération des posts de l'utilisateur
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    getPosts: function(req, res, next) {
        console.log('getPosts Users: ------------------------------------');
        try {
            console.log(req.params.id);
            // On intéroge les données
            UsrSvc.getUserPosts(req.params.id).then((r) => {
                    console.log(r);
                    // On transmet le resultat au front
                    res.status(200).json({ posts: r });
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch((e) => { res.status(500).json({ e }); });
        } catch (e) {
            //fatality
            res.status(500).json({ e });
        }
        console.log('getPosts Users: ------------------------------------');
    },
    /**
     * Supression utilisateur
     * @param {Request} req Request HTTP entrante
     * @param {Response} res Response HTTP sortante
     * @param {Function} next appel aux autres middleware
     */
    deleteOne: function(req, res, next) {
        console.log('deleteOne Users: ------------------------------------');
        try {
            console.log(req.params.id);
            // On modifie les données
            UsrSvc.deleteUser(req.params.id).then((r) => {
                    console.log(r);
                    // On transmet le resultat au front
                    res.status(200).json({ result: r });
                }) // en cas d'erreur on renvois une erreur 500 au front
                .catch((e) => { res.status(500).json({ e }); });
        } catch (e) {
            //fatality
            res.status(500).json({ e });
        }
        console.log('deleteOne Users: ------------------------------------');
    },
}