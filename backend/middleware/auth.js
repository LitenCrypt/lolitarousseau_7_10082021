const jwt = require('jsonwebtoken');
const { UsrSvc } = require('../services/users.svc');
/**
 * Middle xware validant l'authent d'un utilisateur connecté
 * @param {Request} req Request HTTP entrante
 * @param {Response} res Response HTTP sortante
 * @param {Function} next appel aux autres middleware
 */
module.exports = (req, res, next) => {
    console.log('Auth MDW------------------------------------');
    try {
        // Récupére le token dans le header authent
        const token = req.headers.authorization.split(' ')[1];
        //console.log(token);
        // Decode le token
        const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');
        //console.log(decodedToken);
        // stocke la valeur du userID
        const userId = decodedToken.login;
        //console.log(userId);
        // Intéroge les données
        UsrSvc.getUser(userId).then((r) => {
                // Utilisateur présent en BDD
                if (r != null) {
                    console.log('auth::Valid user ID');
                    // Passe au middleware suivant
                    next();
                } else {
                    // léve une erreur
                    console.log('auth::Invalid user ID');
                    throw 'Invalid user ID';
                }
            }) // en cas d'erreur on renvois une erreur 401:Non autorisé au front
            .catch((e) => { res.status(401).json({ e }); });
    } catch {
        //fatality
        res.status(500).json({
            error: new Error('Invalid request!')
        });
    }
    console.log('Auth MDW------------------------------------');
};