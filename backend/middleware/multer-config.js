const multer = require('multer');
/**
 * Configuration des MIME types autorisés pour ce serveur
 */
const MIME_TYPES = {
    'image/jpg': 'jpg',
    'image/jpeg': 'jpg',
    'image/png': 'png',
    'image/gif': 'gif'
};
/**
 * Taille maximum des images
 */
const maxSize = 4 * 4000 * 4000;
/**
 * Configuration du stockage pour le plugin multer
 */
const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, 'images');
    },
    filename: (req, file, callback) => {
        console.log(file);
        // On récupére l'extensipon par rapport au MIME Ttype
        const extension = MIME_TYPES[file.mimetype];
        // On cré le nom basé sur un chaine en base 64 contenant la date du moment et le nom originale du fichier
        const name = Buffer.from(Date.now() + file.originalname).toString('base64') + '.' + extension;
        callback(null, name);
    }
});

module.exports = multer({ storage: storage, limits: { fileSize: maxSize } }).single('image');