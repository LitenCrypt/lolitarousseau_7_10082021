/**
 * OBjet representant un commentaire en BDD
 */
const CommentDto = {
    /**
     * Id en BDD
     */
    id: Number,
    /**
     * Valeur du commentaire
     */
    Msg: String,
    /**
     * Date du commentaire
     */
    Date: String,
    /**
     * @deprecated Non demandé fonctionel
     */
    ReplyTo: Number,
    /**
     * Utilisateur créateur du commentaire
     */
    UserId: String,
    /**
     * ID du post associé
     */
    PostId: Number,
    /**
     * Nom de l'utilisateur créateur du commentaire
     */
    LastName: String,
    /**
     * Nom de l'utilisateur créateur du commentaire
     */
    FirstName: String,
    /**
     * Nom du service de l'utilisateur créateur du commentaire
     */
    SvcName: String,
    /**
     * ID du service de l'utilisateur créateur du commentaire
     */
    SvcId: Number,
    /**
     * avatar de l'utilisateur créateur du commentaire
     */
    ImgUrl: String,
}
exports.CommentDto = CommentDto;