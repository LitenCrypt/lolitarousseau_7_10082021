/**
 * OBjet representant un Post en BDD
 */
const PostDto = {
    /**
     * Id du post
     */
    Id: Number,
    /**
     * Titre du post
     */
    Title: String,
    /**
     * Image associé au post
     */
    GifUrl: String,
    /**
     * Date du  post
     */
    Date: Date,
    /**
     * Id de l'utilisateur associé au post
     */
    UserId: String,
    /**
     * Nom de l'utilisateur créateur du commentaire
     */
    LastName: String,
    /**
     * Nom de l'utilisateur créateur du commentaire
     */
    FirstName: String,
    /**
     * Nom du service de l'utilisateur créateur du commentaire
     */
    SvcName: String,
    /**
     * Id du service de l'utilisateur créateur du commentaire
     */
    SvcId: Number,
    /**
     * avatar de l'utilisateur créateur du commentaire
     */
    ImgUrl: String,
    /**
     * Nombre de commentaires associés aux posts
     */
    CommentCount: Number
}
exports.PostDto = PostDto;