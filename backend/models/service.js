/**
 * OBjet representant un service en BDD
 */
const ServiceDto = {
    /**
     * Id du service
     */
    Id: String,
    /**
     * Nom du service
     */
    Name: String
};

exports.ServiceDto = ServiceDto;