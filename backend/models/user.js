 /**
  * OBjet representant un Utilisateur en BDD
  */
 const UserDto = {
     /**
      * Id de l'utilisateur 
      */
     UserId: String,
     /**
      * Nom de l'utilisateur 
      */
     LastName: String,
     /**
      * Prénom de l'utilisateur 
      */
     FirstName: String,
     /**
      * Nom du service de l'utilisateur 
      */
     SvcName: String,
     /**
      * Id Du service de l'utilisateur 
      */
     SvcId: Number,
     /**
      * Avatar de l'utilisateur 
      */
     ImgUrl: String,
     /**
      * Email de l'utilisateur 
      */
     Email: String,
     /**
      * MDP de l'utilisateur 
      */
     Pwd: String,
     /**
      * L'utilisateur peut il suprimer les posts 
      */
     IsAdmin: Boolean,
     /**
      * nombre de posts de l'utilisateur 
      */
     PostCount: Number
 };

 exports.UserDto = UserDto;