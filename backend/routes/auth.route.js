const express = require('express');
const router = express.Router();
const { UserCtrl } = require('../controllers/user.ctrl');

const multer = require('../middleware/multer-config');

router.post('/signup', multer, UserCtrl.signup);
router.post('/login', UserCtrl.login);

module.exports = router;