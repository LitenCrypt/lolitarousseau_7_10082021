const express = require('express');
const router = express.Router();
const { ParamsCtrl } = require('../controllers/params.ctrl');

const auth = require('../middleware/auth');

router.get('/svc', ParamsCtrl.getServices);
module.exports = router;