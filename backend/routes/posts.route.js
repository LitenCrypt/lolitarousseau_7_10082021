const express = require('express');
const router = express.Router();
const { PostCtrl } = require('../controllers/posts.ctrl');
const multer = require('../middleware/multer-config');
const auth = require('../middleware/auth');



router.get('/', auth, PostCtrl.get);
router.get('/:id', auth, PostCtrl.getOne);
router.post('/', auth, multer, PostCtrl.insertOne);
router.put('/:id', auth, multer, PostCtrl.updateOne);
router.delete('/:id', auth, PostCtrl.deleteOne);
router.get('/:id/comments', auth, PostCtrl.getComments);
router.post('/:id/comments', auth, PostCtrl.addComment);
router.delete('/:id/comments/:cid', auth, PostCtrl.deleteComment);

module.exports = router;