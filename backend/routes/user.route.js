const express = require('express');
const router = express.Router();
const { UserCtrl } = require('../controllers/user.ctrl');
const auth = require('../middleware/auth');

router.get('/:id/posts', auth, UserCtrl.getPosts);
router.get('/:id', auth, UserCtrl.getOne);
router.get('/', auth, UserCtrl.get);
router.put('/:id', auth, UserCtrl.updateOne);
router.delete('/:id', auth, UserCtrl.deleteOne);

module.exports = router;