const { Pool } = require('pg');
/**
 * Class e de base pour la gestion de la relation avec la DB
 * porte la gestion de la connexion et de son paramétrage
 */
exports.dbSvc = {
    /**
     * Pool de connection local
     */
    pool: Pool,
    /**
     * Requete simple sans paramétre
     * @param {String} query Requete SQL sous forme de string
     * @returns {Promise<any>} Set de resultat obtenu en DB ou null
     */
    query: function(query) {
        return new Promise((resolve, reject) => {
            this.pool = new Pool({
                user: process.env.PGUSER,
                password: process.env.PGPASSWORD,
                host: process.env.PGHOST,
                port: process.env.PGPORT,
                database: process.env.PGDATABASE
            });
            console.log('Pool de connexion réalisé');
            this.pool.query(query, (err, results) => {
                console.log('Requete Sql traitée');
                if (err) {
                    console.error("ERROR: " + err);
                    reject(err)
                }
                resolve(results.rows);
            });
        });
    },
    /**
     * requete avec paramétres
     * @param {String} query Requete SQL sous forme de string 
     * @param {[String]} params tableau de paramétre dans
     * @returns {Promise<any>} Resultat ou null
     */
    queryWithParams: function(query, params) {
        return new Promise((resolve, reject) => {
            this.pool = new Pool({
                user: process.env.PGUSER,
                password: process.env.PGPASSWORD,
                host: process.env.PGHOST,
                port: process.env.PGPORT,
                database: process.env.PGDATABASE
            });
            console.log('Pool de connexion réalisé');
            this.pool.query(query, params, (err, results) => {
                console.log('Requete Sql traitée');
                if (err) {
                    console.error("ERROR: ", err);
                    reject(err)
                }
                resolve(results.rows);
            });
        });
    }
}