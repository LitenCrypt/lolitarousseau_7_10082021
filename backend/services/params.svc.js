const { dbSvc } = require('./base.svc');
const { ServiceDto } = require('../models/service');
/**
 * Requete de selection des services, SQL
 */
const selectServiceQuery = 'SELECT "ID", "SVC_NAME" FROM "AppUsr"."Services";';
/**
 * Service permettant la récupération du paramétrage
 */
exports.ParamsSvc = {
    dataSource: dbSvc,
    /**
     * Récupéres les services administratif de l'entreprise
     * @returns {Promise<ServiceDto>} Liste des services trouvés en base
     */
    getAllServices: function() {
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            // Appel de la connection SQL avec la requete select
            this.dataSource.query(selectServiceQuery).then((r) => {
                    var result = [];
                    // pour toutes les lignes trouvées
                    r.forEach(e => {
                        var p = Object.create(ServiceDto);
                        p.Id = e.ID;
                        p.Name = e.SVC_NAME;
                        result.push(p);
                    });
                    // résolution de la promesse (then)
                    resolve(result);
                }) //rejet de la promesse en cas d'erreur
                .catch(reject);
        });

    }
};