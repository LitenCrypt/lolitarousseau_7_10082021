const { PostDto } = require('../models/posts');
const { CommentDto } = require('../models/comment');
const { dbSvc } = require('./base.svc');
/**
 * Requete de récupération des posts, SQL, non paramétrées
 */
const selectPostsQuery = 'SELECT p."ID", p."TITLE", p."GIF_URL", p."DATE", p."USER_ID",' +
    'u."FIRST_NAME", u."LAST_NAME", u."SVC_ID", u."IMG_URL",' +
    's."SVC_NAME", (SELECT COUNT(c."ID") FROM "AppUsr"."Comments" c WHERE c."POST_ID" = p."ID")  as "COMMENT_COUNT" ' +
    'FROM "AppUsr"."Posts" p ' +
    'inner join "AppUsr"."Users" u on p."USER_ID" = u."USER_ID" ' +
    'inner join "AppUsr"."Services" s on u."SVC_ID" = s."ID";';
/**
 * requete de récupération d'un post, SQL, paramétrées
 */
const selectPostQuery = 'SELECT p."ID", p."TITLE", p."GIF_URL", p."DATE", p."USER_ID",' +
    'u."FIRST_NAME", u."LAST_NAME", u."SVC_ID", u."IMG_URL",' +
    's."SVC_NAME", (SELECT COUNT(c."ID") FROM "AppUsr"."Comments" c WHERE c."POST_ID" = p."ID")  as "COMMENT_COUNT" ' +
    'FROM "AppUsr"."Posts" p ' +
    'inner join "AppUsr"."Users" u on p."USER_ID" = u."USER_ID" ' +
    'inner join "AppUsr"."Services" s on u."SVC_ID" = s."ID" where p."ID" = $1;';
/**
 * Requete de récupération des commentaires pour un post, SQL, paramétrées
 */
const selectPostCommentsQuery = 'SELECT c."ID", c."USER_ID", c."COMMENT", c."DATE", c."REPLY_TO", c."POST_ID",' +
    'u."FIRST_NAME", u."LAST_NAME", u."SVC_ID", u."IMG_URL",' +
    's."SVC_NAME"' +
    'FROM "AppUsr"."Comments" c ' +
    'inner join "AppUsr"."Users" u on c."USER_ID" = u."USER_ID" ' +
    'inner join "AppUsr"."Services" s on u."SVC_ID" = s."ID" where c."POST_ID" = $1;';
/**
 * Requete de supression d'un post, SQL, paramétrées
 */
const deletePostQuery = 'DELETE FROM "AppUsr"."Posts" WHERE "ID" = $1 RETURNING * ;';
/**
 * Requete de mise à jour d'un post, SQL, paramétrées
 */
const updatePostQuery = 'UPDATE "AppUsr"."Posts" SET "TITLE" = $2, "GIF_URL"= $3,  "DATE"= $4 WHERE "ID" = $1 RETURNING * ;';
/**
 * Requete de création d'un post, SQL, paramétrées
 */
const insertPostQuery = 'INSERT INTO "AppUsr"."Posts" ("TITLE", "GIF_URL", "USER_ID") values ($1,$2,$3) RETURNING * ;';
/**
 * Requte de création d'un commentaire, SQL, paramétrées
 */
const insertPostCommentQuery = 'INSERT INTO "AppUsr"."Comments"("USER_ID", "COMMENT", "POST_ID") VALUES ($1, $2, $3) RETURNING * ;';
/**
 * Requete de supressio n d'un comentaire, SQL, paramétrées
 */
const deletePostCommentQuery = 'DELETE FROM "AppUsr"."Comments" WHERE "ID" = $1 RETURNING * ;';
/**
 * requete de supression de tous les commentaires d'un post, SQL, paramétrées
 */
const deletePostCommentsQuery = 'DELETE FROM "AppUsr"."Comments" WHERE "POST_ID" = $1 RETURNING * ;';
/**
 * Classe de gestion de la table post dans la DB
 */
exports.PostsSvc = {
    /**
     * propriété porteuse de la conexion 
     */
    dataSource: dbSvc,
    /**
     * Récupére la liste de post dans la DB
     * @returns {Promise<[PostDto]>}
     */
    getPosts: function() {
        console.log('Get posts svc');
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            console.log('Get posts svc');
            // on execute la requete avec la connexion sur la DB
            return this.dataSource.query(selectPostsQuery).then((r) => {
                    console.log(r);
                    const result = [];
                    // pour toutes les lignes de resultats
                    r.forEach(e => {
                        result.push(this.mapPost(e));
                    });
                    // on resout la promesse
                    return resolve(result);
                }) // on rejete la promesse
                .catch(reject);
        });

    },
    /**
     * Récupére un seul post dans la BDD
     * @param {Number} postId 
     * @returns {Promise<PostDto>}
     */
    getPost: function(postId) {
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            //Création du tableau de paramétres dans l'ordre utilisé dans la requetes ($1, $2....)
            var params = [];
            params.push(postId);
            // on execute la requete avec la connexion sur la DB
            this.dataSource.queryWithParams(selectPostQuery, params).then((r) => {
                    const result = this.mapPost(r[0]);
                    // on resout la promesse
                    resolve(result);
                }) // on rejete la promesse
                .catch(reject);
        });

    },
    /**
     * récupre les commentaires d'un post
     * @param {Number} postId 
     * @returns {Promise<[CommentDto]>}
     */
    getPostComments: function(postId) {
        return new Promise((resolve, reject) => {
            //Création du tableau de paramétres dans l'ordre utilisé dans la requetes ($1, $2....)
            var params = [];
            params.push(postId);
            // on execute la requete avec la connexion sur la DB
            this.dataSource.queryWithParams(selectPostCommentsQuery, params).then((r) => {
                    const result = [];
                    r.forEach(e => {
                        result.push(this.mapComments(e));
                    });
                    // on resout la promesse
                    resolve(result);
                }) // on rejete la promesse
                .catch(reject);
        });

    },
    /**
     * Ajoute un commentaire à un post pour un utiistaeur
     * @param {CommentDto} comment 
     * @returns {Promise<Number>} Id du commentaire
     */
    addPostComments: function(comment) {
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            //Création du tableau de paramétres dans l'ordre utilisé dans la requetes ($1, $2....)
            var params = [];
            params.push(comment.UserId);
            params.push(comment.Msg); //Buffer.from().toString('base64')
            params.push(comment.PostId);
            // on execute la requete avec la connexion sur la DB
            this.dataSource.queryWithParams(insertPostCommentQuery, params).then((r) => {
                    console.log('Commentaire inseré');
                    console.log(r);
                    // on resout la promesse
                    resolve(r[0].ID);
                }) // on rejete la promesse
                .catch(reject);
        });

    },
    /**
     * Supression d'un post
     * @param {Number} id 
     * @returns {Promise<Number>} Id du commentaire supprimé
     */
    deletePostsComment: function(id) {
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            //Création du tableau de paramétres dans l'ordre utilisé dans la requetes ($1, $2....)
            var params = [];
            params.push(id);
            // on execute la requete avec la connexion sur la DB
            return this.dataSource.queryWithParams(deletePostCommentQuery, params).then((r) => {
                    console.log('Commentaire éffacé');
                    console.log(r);
                    // on resout la promesse
                    resolve([0].ID);
                }) // on rejete la promesse
                .catch(reject);
        });

    },
    /**
     * Creation d'un post
     * @param {PostDto} post 
     * @returns {Promise<Number>}
     */
    createPost: function(post) {
        return new Promise((resolve, reject) => {
            //Création du tableau de paramétres dans l'ordre utilisé dans la requetes ($1, $2....)
            var params = [];
            params.push(post.Title);
            params.push(post.GifUrl);
            params.push(post.UserId);
            // on execute la requete avec la connexion sur la DB
            return this.dataSource.queryWithParams(insertPostQuery, params).then((r) => {
                    console.log('Post inseré');
                    // on resout la promesse
                    console.log(r);
                    resolve([0].ID);
                }) // on rejete la promesse
                .catch(reject);
        });
    },
    /**
     * Mise à jour d'un post
     * @param {PostDto} post 
     * @returns {Promise<Number>} Id du post
     */
    updatePost: function(post) {
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            //Création du tableau de paramétres dans l'ordre utilisé dans la requetes ($1, $2....)
            var params = [];
            params.push(post.Title);
            params.push(post.GifUrl);
            params.push(post.UserId);
            // on execute la requete avec la connexion sur la DB
            this.dataSource.queryWithParams(updatePostQuery, params).then((r) => {
                    console.log('Post mis à jour');
                    console.log(r);
                    // on resout la promesse
                    resolve([0].ID);
                }) // on rejete la promesse
                .catch(reject);
        });
    },
    /**
     * Supression d'un post
     * @param {Number} id 
     * @returns {Promise<Number>} ID du post
     */
    deletePost: function(id) {
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            //Création du tableau de paramétres dans l'ordre utilisé dans la requetes ($1, $2....)
            var params = [];
            params.push(id);
            // on execute la requete avec la connexion sur la DB
            // supression de commentaires lié au post relation FK_COMMENT_POST
            this.dataSource.queryWithParams(deletePostCommentsQuery, params).then((r) => {
                    console.log(r);
                    // on execute la requete avec la connexion sur la DB
                    //Supression des posts à proprement parlé
                    this.dataSource.queryWithParams(deletePostQuery, params).then((x) => {
                        console.log('Post supprimé');
                        console.log(x);
                        // on resout la promesse
                        resolve(x);
                    });
                }) // on rejete la promesse
                .catch(reject);
        });
    },
    /**
     * Créé un DTO de post pour le front
     * @param {any} i 
     * @returns {PostDto}
     */
    mapPost: function(i) {
        const p = Object.create(PostDto);
        p.Id = i.ID;
        p.Title = i.TITLE;
        p.GifUrl = i.GIF_URL;
        p.Date = i.DATE;
        p.UserId = i.USER_ID;
        p.FirstName = i.FIRST_NAME;
        p.LastName = i.LAST_NAME;
        p.SvcId = i.SVC_ID;
        p.SvcName = i.SVC_NAME;
        p.ImgUrl = i.IMG_URL;
        p.CommentCount = i.COMMENT_COUNT;
        return p;
    },
    /**
     * Créé un DTO de commentaire pour le front
     * @param {any} i 
     * @returns {CommentDto}
     */
    mapComments: function(i) {
        const p = Object.create(CommentDto);
        p.Id = i.ID;
        p.Msg = i.COMMENT, //Buffer.from(, 'base64').toString('ascii');
            p.PostId = i.POST_ID;
        p.Date = i.DATE;
        p.UserId = i.USER_ID;
        p.FirstName = i.FIRST_NAME;
        p.LastName = i.LAST_NAME;
        p.SvcId = i.SVC_ID;
        p.SvcName = i.SVC_NAME;
        p.ImgUrl = i.IMG_URL;
        return p;
    }
};