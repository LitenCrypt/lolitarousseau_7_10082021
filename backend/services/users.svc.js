const { UserDto } = require('../models/user');
const { PostDto } = require('../models/posts');
const { dbSvc } = require('./base.svc');
/**
 * Requete de sélection des utilisateurs de la BDD, SQL, non paramétrées
 */
const selectUsersQuery = 'SELECT u."USER_ID", u."FIRST_NAME", u."LAST_NAME", u."EMAIL", s."SVC_NAME",u."SVC_ID", u."IMG_URL", u."IS_ADMIN", ' +
    '(SELECT COUNT(p."ID") FROM "AppUsr"."Posts" p WHERE p."USER_ID" = u."USER_ID" ) AS "POST_COUNT" FROM "AppUsr"."Users" u inner join "AppUsr"."Services" s on u."SVC_ID" = s."ID";';
/**
 * Requete de sélection d'un utilisateur de la BDD, SQL, paramétrées
 */
const selectUserQuery = 'SELECT u."USER_ID", u."FIRST_NAME", u."LAST_NAME", u."EMAIL", s."SVC_NAME",u."SVC_ID", u."IMG_URL",  u."IS_ADMIN",' +
    '(SELECT COUNT(p."ID") FROM "AppUsr"."Posts" p WHERE p."USER_ID" = u."USER_ID" ) AS "POST_COUNT" FROM "AppUsr"."Users" u inner join "AppUsr"."Services" s on u."SVC_ID" = s."ID" WHERE u."USER_ID" = $1;';
/**
 * Requete de récuppération du mdp d'un utilisateur de la BDD, SQL, paramétrées
 */
const validUserPswdQuery = 'SELECT u."PWD" FROM "AppUsr"."Users" u WHERE u."USER_ID" = $1;';
/**
 * Requete d'insertion d'un utilisateur de la BDD, SQL, paramétrées
 */
const insertUserQuery = 'INSERT INTO "AppUsr"."Users" ("USER_ID", "FIRST_NAME", "LAST_NAME", "EMAIL", "SVC_ID", "PWD", "IMG_URL", "IS_ADMIN") values' +
    '($1,$2,$3,$4,$5,$6,$7,$8);';
/**
 * Requete de mise à jour d'un utilisateur de la BDD, SQL, paramétrées
 */
const updateUserQuery = 'UPDATE "AppUsr"."Users" SET ' +
    ' "FIRST_NAME" = $1, ' +
    ' "LAST_NAME"= $2, ' +
    ' "EMAIL"= $3,' +
    ' "SVC_ID"= $4,' +
    ' "IMG_URL"= $5' +
    ' WHERE "USER_ID" = $6;';
/**
 * Requete de supression d'un utilisateur de la BDD, SQL, paramétrées
 */
const deleteUserQuery = 'DELETE FROM "AppUsr"."Users" WHERE "USER_ID" = $1;';
/**
 * Requete de mise à jour du mdp d'un utilisateur de la BDD, SQL, paramétrées
 */
const updateUserPwdQuery = 'UPDATE "AppUsr"."Users" SET ' +
    ' "PWD"= $1' +
    ' WHERE "USER_ID" = $2;';
/**
 * Requete de sélection des posts d'un utilisateur de la BDD, SQL, paramétrées
 */
const selectUserPostsQuery = 'SELECT p."ID", p."TITLE", p."GIF_URL", p."DATE", p."USER_ID",' +
    'u."FIRST_NAME", u."LAST_NAME", u."SVC_ID", u."IMG_URL",' +
    's."SVC_NAME"' +
    'FROM "AppUsr"."Posts" p ' +
    'inner join "AppUsr"."Users" u on p."USER_ID" = u."USER_ID" ' +
    'inner join "AppUsr"."Services" s on u."SVC_ID" = s."ID" where p."USER_ID" = $1;';
/**
 * Requete de supression des posts d'un utilisateur de la BDD, SQL, paramétrées
 */
const deleteUserPostQuery = 'DELETE FROM "AppUsr"."Posts" WHERE "USER_ID" = $1 RETURNING * ;';
/**
 * Requete de supression des commentaires d'un utilisateur de la BDD, SQL, paramétrées
 */
const deleteUserCommentsQuery = 'DELETE FROM "AppUsr"."Comments" WHERE "USER_ID" = $1 RETURNING * ;';
/**
 * Classe de gestion de la table Users dans la DB
 */
exports.UsrSvc = {
    /**
     * Connexion avec la BDD
     */
    dataSource: dbSvc,
    /**
     * Récupération de tous les utilisateurs de la BDD
     * @returns {Promise<[UserDto]>}
     */
    getAllUsers: function() {
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            // on execute la requete avec la connexion sur la DB
            this.dataSource.query(selectUsersQuery).then((r) => {
                    console.log(r);
                    // On construit le jeux de resultats
                    var result = [];
                    r.forEach(e => {
                        result.push(this.mapUser(e));
                    });
                    // on resout la promesse
                    resolve(result);
                }) // on rejete la promesse
                .catch(reject);
        });
    },
    /**
     * Récupération d'un utilisateur de la BDD
     * @param {String} userId 
     * @returns {Promise<UserDto>}
     */
    getUser: function(userId) {
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            //Création du tableau de paramétres dans l'ordre utilisé dans la requetes ($1, $2....)
            var params = [];
            params.push(userId);
            // on execute la requete avec la connexion sur la DB
            this.dataSource.queryWithParams(selectUserQuery, params).then((r) => {
                    if (r != null) {
                        // On construit le jeux de resultats
                        result = this.mapUser(r[0]);
                        // on resout la promesse
                        resolve(result);
                    } else {
                        // on rejete la promesse
                        reject(null);
                    }
                }) // on rejete la promesse
                .catch(reject);
        });
    },
    /**
     * Récupération de tous les post d'un utilisateur
     * @param {String} userId 
     * @returns {Promise<PostDto>}
     */
    getUserPosts: function(userId) {
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            //Création du tableau de paramétres dans l'ordre utilisé dans la requetes ($1, $2....)
            var params = [];
            params.push(userId);
            // on execute la requete avec la connexion sur la DB
            this.dataSource.queryWithParams(selectUserPostsQuery, params).then((r) => {
                console.log(r);
                // On construit le jeux de resultats
                const result = [];
                r.forEach(e => {
                    result.push(this.mapPost(e));
                });
                console.log(result);
                // on resout la promesse
                resolve(result);
            }).catch(reject);
        }); // on rejete la promesse

    },
    /**
     * Création d'un utilisateur
     * @param {UserDto} user 
     * @returns {Promise<UserDto>}
     */
    createUser: function(user) {
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            // encodage du mail et de l'id en base 64 
            const id = Buffer.from(user.Email).toString('base64');
            //Création du tableau de paramétres dans l'ordre utilisé dans la requetes ($1, $2....)
            const params = [];
            params.push(id, user.FirstName, user.LastName, id, user.SvcId, user.Pwd, user.ImgUrl, false);
            console.log(params);
            // on execute la requete avec la connexion sur la DB
            return this.dataSource.queryWithParams(insertUserQuery, params).then(r => {
                    console.log('Insertion réussie: ' + r);
                    this.getUser(id).then((x) => {
                        // on resout la promesse
                        resolve(x);
                    }).catch((e) => {
                        // on rejete la promesse
                        reject(e);
                    });
                }) // on rejete la promesse
                .catch(reject);
        });

    },
    /**
     * Mis à jour d'un utilisateur
     * retiurne -> ID de l'utilisateur mis à jour
     * @param {UserDto} user 
     * @returns {Promise<Number>} 
     */
    updateUser: function(user) {
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            //Création du tableau de paramétres dans l'ordre utilisé dans la requetes ($1, $2....)
            var params = [];
            params.push(user.FirstName);
            params.push(user.LastName);
            // Encodage du mail en base 64
            params.push(Buffer.from(user.Email).toString('base64'));
            params.push(user.SvcId);
            params.push(user.ImgUrl);
            params.push(user.UserId);
            // on execute la requete avec la connexion sur la DB
            return this.dataSource.queryWithParams(updateUserQuery, params).then((r) => {
                    console.log('Mise à jour réussie');
                    // on resout la promesse
                    resolve(r);
                }) // on rejete la promesse
                .catch(reject);
        });

    },
    /**
     * Supression d'un utilisateur
     * retiurne -> ID de l'utilisateur mis à jour
     * @param {UserDto} user 
     * @returns {Promise<Number>} 
     */
    deleteUser: function(userId) {
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            //Création du tableau de paramétres dans l'ordre utilisé dans la requetes ($1, $2....)
            var params = [];
            params.push(userId);
            // on execute la requete avec la connexion sur la DB
            // SUpression des commentaires relation FK_COMMENT_USER
            this.dataSource.queryWithParams(deleteUserCommentsQuery, params).then((r) => {
                    console.log('Suppression réussie');
                    // on execute la requete avec la connexion sur la DB
                    // Suppresssion des posts relation FK_POST_USER
                    this.dataSource.queryWithParams(deleteUserPostQuery, params).then((r) => {
                            console.log('Suppression réussie');
                            // on execute la requete avec la connexion sur la DB
                            // Supression des utilisateurs à proprement parlé
                            this.dataSource.queryWithParams(deleteUserQuery, params).then((r) => {
                                    console.log('Suppression réussie');
                                    // on resout la promesse
                                    resolve(r)
                                }) // on rejete la promesse
                                .catch(reject);
                        }) // on rejete la promesse
                        .catch(reject);
                }) // on rejete la promesse
                .catch(reject);
        });
    },
    /**
     * Mis à jour du mdp utilisateur
     * @param {String} userId 
     * @param {String} pswd 
     * @returns {Promise<any>}
     */
    changeUserPswd: function(userId, pswd) {
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            //Création du tableau de paramétres dans l'ordre utilisé dans la requetes ($1, $2....)
            var params = [];
            params.push(pswd);
            params.push(userId);
            return this.dataSource.queryWithParams(updateUserPwdQuery, params, (r) => {
                    console.log('Mise à jour réussie');
                    // on resout la promesse
                    resolve(r)
                }) // on rejete la promesse
                .catch(reject);
        });

    },
    /**
     * Récupération du mdp d'un  utilisateur
     * @param {String} userId 
     * @returns {Promise<String>}
     */
    getUserPswd: function(userId) {
        // On emballe dans une promesse
        return new Promise((resolve, reject) => {
            //Création du tableau de paramétres dans l'ordre utilisé dans la requetes ($1, $2....)
            var params = [];
            params.push(userId);
            // on execute la requete avec la connexion sur la DB
            return this.dataSource.queryWithParams(validUserPswdQuery, params).then((r) => {
                    console.log(r);
                    if (r.length == 1) {
                        console.log('Validation UID réussie');
                        // on resout la promesse
                        // en retorunant la valeur du mot de passe
                        resolve(r[0].PWD);
                    } else {
                        // on rejete la promesse
                        reject(false);
                    }

                }) // on rejete la promesse
                .catch(reject);
        });
    },
    /**
     * Créé un DTO utilisateur pour le front
     * @param {any} e 
     * @returns {UserDto} filled
     */
    mapUser: function(e) {
        var p = Object.create(UserDto);
        p.UserId = e.USER_ID;
        p.FirstName = e.FIRST_NAME;
        p.LastName = e.LAST_NAME;
        p.Email = Buffer.from(e.EMAIL, 'base64').toString('ascii');
        p.SvcId = e.SVC_ID;
        p.SvcName = e.SVC_NAME;
        p.ImgUrl = e.IMG_URL;
        p.PostCount = e.POST_COUNT;
        p.IsAdmin = e.IS_ADMIN;
        return p;
    },
    /**
     * Créé un DTO de post pour le front
     * @param {any} i 
     * @returns {PostDto}
     */
    mapPost: function(i) {
        const p = Object.create(PostDto);
        p.Id = i.ID;
        p.Title = i.TITLE;
        p.GifUrl = i.GIF_URL;
        p.Date = i.DATE;
        p.UserId = i.USER_ID;
        p.FirstName = i.FIRST_NAME;
        p.LastName = i.LAST_NAME;
        p.SvcId = i.SVC_ID;
        p.SvcName = i.SVC_NAME;
        p.ImgUrl = i.IMG_URL;
        return p;
    }
};