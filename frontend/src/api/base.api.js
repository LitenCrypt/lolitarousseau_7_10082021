//const userLocalStorageKey = 'MY';
const tokenLocalStorageKey = 'token';
const BaseApi = {
    baseApiUrl: 'http://localhost:3000/api',
    postRoute: 'posts',
    usersRoute: 'users',
    authRoute: 'auth',
    paramsRoute: 'params',
    getJsonHeaders: function() {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'JWT ' + BaseApi.getToken()
        }
        return { headers: headers };
    },
    getFormHeaders: function() {
        const headers = {
            //'Content-Type': 'multipart/form-data',
            'Authorization': 'JWT ' + BaseApi.getToken()
        }
        return { headers: headers };
    },
    getToken: function() {
        return localStorage.getItem(tokenLocalStorageKey);
    },
    getBaseUsersRoute: function() {
        return this.baseApiUrl + '/' + this.usersRoute;
    },
    getBasePostsRoute: function() {
        return this.baseApiUrl + '/' + this.postRoute;
    },
    getBaseAuthRoute: function() {
        return this.baseApiUrl + '/' + this.authRoute;
    },
    getBaseParamsRoute: function() {
        return this.baseApiUrl + '/' + this.paramsRoute;
    }
}
export default BaseApi;