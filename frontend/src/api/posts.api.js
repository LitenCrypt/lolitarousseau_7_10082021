import axios from 'axios';
import BaseApi from './base.api';

const PostsApi = {
    getAll: function() {
        return axios.get(BaseApi.getBasePostsRoute(), BaseApi.getJsonHeaders());
    },
    get: function(postId) {
        return axios.get(BaseApi.getBasePostsRoute() + '/' + postId, BaseApi.getJsonHeaders());
    },
    add: function(post) {
        return axios.post(BaseApi.getBasePostsRoute(), post, BaseApi.getFormHeaders());
    },
    update: function(postId, post) {
        return axios.put(BaseApi.getBasePostsRoute() + '/' + postId, post, BaseApi.getFormHeaders());
    },
    delete: function(postId) {
        console.log(postId);
        return axios.delete(BaseApi.getBasePostsRoute() + '/' + postId, BaseApi.getJsonHeaders());
    },
    getComments: function(postId) {
        return axios.get(BaseApi.getBasePostsRoute() + '/' + postId + '/comments', BaseApi.getJsonHeaders());
    },
    addComments: function(postId, data) {
        return axios.post(BaseApi.getBasePostsRoute() + '/' + postId + '/comments', { comment: data }, BaseApi.getJsonHeaders());
    },
    deleteComments: function(postId, commentId) {
        return axios.delete(BaseApi.getBasePostsRoute() + '/' + postId + '/comments/' + commentId, BaseApi.getJsonHeaders());
    },
}
export default PostsApi;