import axios from 'axios';
import BaseApi from './base.api';

/**
 * Gestion des données statiques et de l'utilisateur connecté
 */
const ProfileApi = {
    /**
     * 
     * @returns {[ServiceDto]}
     */
    getAllServices: function() {
        return axios.get(BaseApi.getBaseParamsRoute() + '/svc');
    },
    /**
     * 
     * @param {String} email 
     * @param {String} pwd 
     * @returns {UserDto}
     */
    loginUser: function(email, pwd) {
        return axios.post(BaseApi.getBaseAuthRoute() + '/login', { login: email, pwd: pwd });
    },
    /**
     * Créé un nouvel utilisateur, l'authentifie et récupére son profil
     * @returns {Promise<UserDto>} l'utilisateur créé
     */
    signupUser: function(user) {
        return axios.post(BaseApi.getBaseAuthRoute() + '/signup', user);
    },
    /**
     * Créé un nouvel utilisateur, l'authentifie et récupére son profil
     * @returns {Promise<UserDto>} l'utilisateur créé
     */
    deleteUser: function(userId) {
        return axios.delete(BaseApi.getBaseUsersRoute() + '/' + userId, BaseApi.getJsonHeaders());
    }
}
export default ProfileApi;