import axios from 'axios';
import BaseApi from './base.api';

const baseApiUrl = 'http://localhost:3000/api';
const localApiRoute = 'users';
/**
 * Gestion des données statiques et de l'utilisateur connecté
 */
const UsersApi = {
    getAll: function() {
        return axios.get(BaseApi.getBaseUsersRoute(), BaseApi.getJsonHeaders());
    },
    get: function(id) {
        return axios.get(BaseApi.getBaseUsersRoute() + '/' + id, BaseApi.getJsonHeaders());
    },
    update: function(id, user) {
        return axios.put(BaseApi.getBaseUsersRoute(), user, BaseApi.getJsonHeaders());
    },
    getPosts: function(id) {
        return axios.get(BaseApi.getBaseUsersRoute() + '/' + id + '/posts', BaseApi.getJsonHeaders());
    }
}
export default UsersApi;