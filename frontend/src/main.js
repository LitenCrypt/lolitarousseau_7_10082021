import { createApp } from 'vue'
import main from './App.vue'
import router from './router'
import store from './store'

const app = createApp(main);
app.use(router);
app.use(store);

app.mount('#app')