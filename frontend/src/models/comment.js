const CommentDto = {
    id: Number,
    Msg: String,
    Date: String,
    ReplyTo: Number,
    UserId: String,
    PostId: Number,
    LastName: String,
    FirstName: String,
    SvcName: String,
    SvcId: Number,
    ImgUrl: String,
}
export default CommentDto;