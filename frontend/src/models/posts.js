import CommentDto from "./comment";
const PostDto = {
    Id: Number,
    Title: String,
    GifUrl: String,
    Date: Date,
    UserId: String,
    LastName: String,
    FirstName: String,
    SvcName: String,
    SvcId: Number,
    ImgUrl: String,
    CommentCount: Number,
    Comments: [CommentDto]
}
export default PostDto;