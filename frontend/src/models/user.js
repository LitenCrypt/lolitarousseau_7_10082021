 const UserDto = {
     UserId: String,
     LastName: String,
     FirstName: String,
     SvcName: String,
     SvcId: Number,
     ImgUrl: String,
     Email: String,
     Pwd: String,
     IsAdmin: Boolean,
     PostCount: Number,
 };
 export default UserDto;