import { createRouter, createWebHashHistory } from 'vue-router'
import home from '../views/home.vue'
import login from '../views/login.vue'

const routes = [{
        path: '/home',
        name: 'home',
        component: home
    },
    {
        path: '/',
        name: 'auth',
        component: login
    }


]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router