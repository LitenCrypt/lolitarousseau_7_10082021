import ProfileStore from './modules/profile'
import PostStore from './modules/posts'
import UsersStore from './modules/users'
import { createStore } from 'vuex'
const debug = process.env.NODE_ENV !== 'production'

const store = createStore({
    modules: {
        ProfileStore,
        PostStore,
        UsersStore
    },
    strict: debug,
});
export default store;