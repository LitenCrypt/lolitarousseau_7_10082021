import PostsApi from '../../api/posts.api'
import CommentDto from '../../models/comment';
import PostDto from '../../models/posts';

const postsLocalStorageKey = 'POSTS';
// Create a new store definition.
const PostStore = {
    state: {
        allPosts: [PostDto],
        selectedPost: PostDto,
        postComments: [CommentDto]
    },
    getters: {
        getPosts(state) {
            console.log(state.allPosts);
            const user = state.allPosts;
            if (!user) {
                return [];
            } else { return state.allPosts; }
        }
    },
    actions: {
        getAllPosts({ commit }) {
            return new Promise((resolve, reject) => {
                PostsApi.getAll().then(response => {
                    console.log(response.data.posts);
                    commit('SET_POSTS', response.data.posts);
                    resolve(response.data.posts);
                }).catch((e) => {
                    console.log(e);
                    reject(e);
                });
            });
        },
        getComments({ commit }, data) {
            return new Promise((resolve, reject) => {
                PostsApi.getComments(data).then(response => {
                    console.log(response.data.comments);
                    commit('SET_COMMENTS', { pid: data, comments: response.data.comments });
                    resolve(response.data.comments);
                }).catch((e) => {
                    console.log(e);
                    reject(e);
                });
            });
        },
        deletePost({ commit }, data) {
            return new Promise((resolve, reject) => {
                console.log(data);
                PostsApi.delete(data).then(response => {
                    console.log('Post deleted');
                    console.log(response.data);
                    commit('REMOVE_POST', data);
                    resolve(data);
                }).catch((e) => {
                    console.log(e);
                    reject(e);
                });
            });
        },
        updatePost({ commit }, data) {
            return new Promise((resolve, reject) => {
                console.log(data);
                PostsApi.update(data).then(response => {
                    console.log(response.data.posts);
                    commit('UPDATE_POST', response.data.posts);
                    resolve(response.data.posts);
                }).catch((e) => {
                    console.log(e);
                    reject(e);
                });
            });
        },
        addPost({ commit }, data) {
            return new Promise((resolve, reject) => {
                console.log(data);
                PostsApi.add(data.form).then(response => {
                    console.log('Post added');
                    console.log(response.data);
                    commit('ADD_POST', data.post);
                    resolve(data);
                }).catch((e) => {
                    console.log(e);
                    reject(e);
                });
            });
        },
        addComment({ commit }, data) {
            return new Promise((resolve, reject) => {
                console.log(data);
                PostsApi.addComments(data.pid, data.cmt).then(response => {
                    console.log('Comment added');
                    console.log(response.data);
                    commit('ADD_COMMENT', data);
                    resolve(data);
                }).catch((e) => {
                    console.log(e);
                    reject(e);
                });
            });
        },
        deleteComment({ commit }, data) {
            return new Promise((resolve, reject) => {
                console.log(data);
                PostsApi.deleteComments(data.postId, data.cmtId).then(response => {
                    console.log('Comment deleted');
                    console.log(response.data);
                    commit('REMOVE_COMMENT', data);
                    resolve(data);
                }).catch((e) => {
                    console.log(e);
                    reject(e);
                });
            });
        }
    },
    mutations: {
        /**
         * Stocke le token dans le localstorage et passe la valeur de isauth a true
         * @param {*} state 
         * @param {String} token 
         */
        SET_POSTS(state, posts) {
            localStorage.setItem(postsLocalStorageKey, JSON.stringify(posts));
            state.allPosts = posts;
        },
        /**
         * Stocke le token dans le localstorage et passe la valeur de isauth a true
         * @param {*} state 
         * @param {String} token 
         */
        ADD_POST(state, post) {
            //localStorage.setItem(postsLocalStorageKey, posts);
            state.allPosts.push(post);
        },
        /**
         * Stocke le token dans le localstorage et passe la valeur de isauth a true
         * @param {*} state 
         * @param {String} token 
         */
        UPDATE_POST(state, post) {
            //localStorage.setItem(postsLocalStorageKey, posts);
            state.allPosts = state.allPosts.filter((itm) => {
                return itm.Id != post.Id;
            });
            state.allPosts.push(post);
        },
        /**
         * Stocke le token dans le localstorage et passe la valeur de isauth a true
         * @param {*} state 
         * @param {String} token 
         */
        REMOVE_POST(state, postId) {
            //localStorage.setItem(postsLocalStorageKey, posts);
            state.allPosts = state.allPosts.filter((itm) => {
                return itm.Id != postId;
            });
        },
        /**
         * Stocke le token dans le localstorage et passe la valeur de isauth a true
         * @param {*} state 
         * @param {String} token 
         */
        SET_COMMENTS(state, data) {
            //localStorage.setItem(postsLocalStorageKey, posts);
            var p = state.allPosts.filter((itm) => {
                return itm.Id == data.pid;
            });
            p[0].Comments = data.comments;
        },
        /**
         * Stocke le token dans le localstorage et passe la valeur de isauth a true
         * @param {*} state 
         * @param {String} token 
         */
        ADD_COMMENT(state, data) {
            //localStorage.setItem(postsLocalStorageKey, posts);
            var p = state.allPosts.filter((itm) => {
                return itm.Id == data.pid;
            });
            p[0].Comments.push(data);
        },
        /**
         * Stocke le token dans le localstorage et passe la valeur de isauth a true
         * @param {*} state 
         * @param {String} token 
         */
        REMOVE_COMMENT(state, data) {
            //localStorage.setItem(postsLocalStorageKey, posts);
            const x = state.allPosts.filter((itm) => {
                return itm.Id == data.pid;
            });
            if (x && x.length > 0) {
                x[0].Comments = x[0].Comments.filter((itm) => {
                    return itm.Id == data.cid;
                });
            }
        },
    }
}
export default PostStore;