import ProfileApi from '../../api/profile.api'
import UsersApi from '../../api/users.api'
import ServiceDto from '../../models/service';
import UserDto from '../../models/user';
import PostDto from '../../models/posts';

const tokenLocalStorageKey = 'token';
const userLocalStorageKey = 'MY';
//const myPostsLocalStorageKey = 'MY_POSTS';
// Create a new store definition.
const ProfilStore = {
    state: {
        isAuth: !!localStorage.getItem(tokenLocalStorageKey),
        allSvc: [ServiceDto],
        userPosts: [PostDto],
        connectedUser: JSON.parse(localStorage.getItem(userLocalStorageKey))
    },
    getters: {
        getConnectedUser(state) {
            console.log(state.connectedUser);
            const user = state.connectedUser;
            if (!user) {
                return Object.create(UserDto);
            } else { return state.connectedUser; }
        },
        getUserPosts(state) {
            console.log(state.userPosts);
            const user = state.userPosts;
            if (!user) {
                return [];
            } else { return state.userPosts; }
        },
        getIsAuth(state) {
            console.log(state.isAuth);
            return state.isAuth;
        },
        getServices(state) {
            return state.allSvc;
        }
    },
    actions: {
        /**
         * récupére la liste des services de l'entreprise
         * @param {*} param0 
         */
        getSvc({ commit }) {
            return new Promise((resolve, reject) => {
                ProfileApi.getAllServices()
                    .then(response => {
                        commit('SET_SVC', response.data);
                        resolve(response.data);
                    }).catch((e) => {
                        console.log(e);

                        reject(e);
                    });
            });

        },
        /**
         * Authentifie l'utilisateur et en cas de réussite le stocke dans le store
         * @param {*} param0 
         * @param {String} email 
         * @param {String} pwd 
         */
        logUser({ commit }, data) {
            return new Promise((resolve, reject) => {
                console.log(data);
                ProfileApi.loginUser(data.login, data.pwd).then(response => {
                    console.log(response.data.token);
                    commit('SET_TOKEN', response.data.token)
                    console.log(response.data.user);
                    commit('SET_USR', response.data.user);
                    resolve(response.data.user);
                }).catch((e) => {
                    console.log(e);
                    reject(e);
                });
            });
        },
        /**
         * créé un profil utilisateur coté BDD et l'authentifie coté client
         * @param {*} param0 
         * @param {UserDto} user 
         */
        signup({ commit }, data) {
            return new Promise((resolve, reject) => {
                console.log(data);
                ProfileApi.signupUser(data.form).then(response => {
                    console.log(response.data.token);
                    commit('SET_TOKEN', response.data.token);
                    console.log(response.data.user);
                    commit('SET_USR', response.data.user);
                    resolve(response.data.user);
                }).catch((e) => {
                    console.log(e);
                    reject(e);
                });
            });
        },
        getPosts({ commit }, data) {
            return new Promise((resolve, reject) => {
                console.log(data);
                UsersApi.getPosts(data).then(response => {
                    console.log(response.data.posts);
                    commit('SET_MY_POSTS', response.data.posts);
                    resolve(response.data.posts);
                }).catch((e) => {
                    console.log(e);
                    reject(e);
                });
            });
        },
        logout({ commit }) {
            return new Promise((resolve, reject) => {
                try {
                    commit('DISCONNECT');
                    resolve(false);
                } catch (error) {
                    reject(error);
                }
            });
        },
        signout({ commit }, data) {
            return new Promise((resolve, reject) => {
                try {
                    ProfileApi.deleteUser(data).then((response) => {
                        console.log(response.data);
                        commit('DISCONNECT');
                        resolve(false);
                    }).catch(reject)
                } catch (error) {
                    reject(error);
                }
            });
        },

    },
    mutations: {
        /**
         * stocke les eervices dans l'etat de session local
         * @param {*} state 
         * @param {[ServiceDto]} svc 
         */
        SET_SVC(state, svc) {
            // Si on autorise la session persistante
            //localStorage.setItem('SVC', JSON.stringify(svc));
            state.allSvc = svc;
        },
        /**
         * stocke l'utilisateur connecé dans l'etat de session locale
         * @param {*} state 
         * @param {UserDto} user 
         */
        SET_USR(state, user) {
            // Si on autorise la session persistante
            localStorage.setItem(userLocalStorageKey, JSON.stringify(user));
            console.log(user);
            state.connectedUser = user;
            console.log(state.connectedUser);
            console.log(state.connectedUser.FirstName);
            state.isAuth = true;
        },
        /**
         * Stocke le token dans le localstorage et passe la valeur de isauth a true
         * @param {*} state 
         * @param {String} token 
         */
        SET_TOKEN(state, token) {
            localStorage.setItem(tokenLocalStorageKey, token);
            state.isAuth = true;
        },
        /**
         * Stocke le token dans le localstorage et passe la valeur de isauth a true
         * @param {*} state 
         * @param {String} token 
         */
        SET_MY_POSTS(state, posts) {
            //localStorage.setItem(myPostsLocalStorageKey, JSON.stringify(posts));
            state.posts = posts;
        },
        /**
         * Stocke le token dans le localstorage et passe la valeur de isauth a true
         * @param {*} state 
         * @param {String} token 
         */
        DISCONNECT(state) {
            localStorage.removeItem(tokenLocalStorageKey);
            localStorage.removeItem(userLocalStorageKey);
            state.isAuth = false;
        }
    }
}
export default ProfilStore;