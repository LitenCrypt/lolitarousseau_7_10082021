import UsersApi from '../../api/users.api'
import PostDto from '../../models/posts';

import UserDto from '../../models/user';

const usersLocalStorageKey = 'USERS';
// Create a new store definition.
const UsersStore = {
    state: {
        allUsers: [UserDto],
        selectedUser: PostDto,
        userPosts: [PostDto]
    },
    getters: {
        getUsers(state) {
            console.log(state.allUsers);
            const user = state.allUsers;
            if (!user) {
                return [];
            } else { return state.allUsers; }
        }
    },
    actions: {
        getAllUsers({ commit }) {
            return new Promise((resolve, reject) => {
                UsersApi.getAll().then(response => {
                    console.log(response.data.users);
                    commit('SET_USERS', response.data.users);
                    resolve(response.data.users);
                }).catch((e) => {
                    console.log(e);
                    reject(e);
                });
            });
        },
        setSelectedUser({ commit }, data) {
            return new Promise((resolve, reject) => {
                UsersApi.getAll().then(response => {
                    commit('SET_USERS', data);
                    resolve(data);
                }).catch((e) => {
                    console.log(e);
                    reject(e);
                });
            });
        },
        getPosts({ commit }, data) {
            return new Promise((resolve, reject) => {
                console.log(data);
                UsersApi.getPosts(data).then(response => {
                    console.log(response.data.posts);
                    commit('SET_POSTS', response.data.posts);
                    resolve(response.data.posts);
                }).catch((e) => {
                    console.log(e);
                    reject(e);
                });
            });
        },
    },
    mutations: {
        /**
         * Stocke le token dans le localstorage et passe la valeur de isauth a true
         * @param {*} state 
         * @param {String} token 
         */
        SET_USERS(state, users) {
            localStorage.setItem(usersLocalStorageKey, JSON.stringify(users));
            state.allUsers = users;
        },
        /**
         * Stocke le token dans le localstorage et passe la valeur de isauth a true
         * @param {*} state 
         * @param {String} token 
         */
        SET_USER(state, user) {
            state.selectedUser = user;
        },
        /**
         * Stocke le token dans le localstorage et passe la valeur de isauth a true
         * @param {*} state 
         * @param {String} token 
         */
        SET_POSTS(state, pots) {
            state.userPosts = pots;
        }
    }
}
export default UsersStore;